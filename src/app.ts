import * as App from 'koa';
import * as Views from 'koa-views';
import * as Json from 'koa-json';
import * as Static from 'koa-static';
import * as Path from 'path';
import * as Bodyparser from 'koa-bodyparser';
import * as Helmet from 'koa-helmet';
import router from './routes/routes';

const app = new App();
const onerror = require('koa-onerror');
const logger = require('koa-logger');
const debug = require('debug')('koa2:server');
const config = require('../config');
const port = process.env.PORT || config.port

// error handler
onerror(app);

// middlewares
app.use(Bodyparser())
  .use(Json())
  .use(Helmet())
  .use(logger())
  .use(Static(__dirname + '/public'))
  .use(Views(Path.join(__dirname, '../views'), {
    options: { settings: { views: Path.join(__dirname, '../views') } },
    map: { 'html': 'nunjucks' },
    extension: 'html'
  }))
  .use(router.routes())
  .use(router.allowedMethods());

// logger
app.use(async (ctx: App.Context, next: Function) => {
  const start = new Date().getMilliseconds();
  await next();
  const ms = new Date().getMilliseconds() - start;
  console.log(`${ctx.method} ${ctx.url} - $ms`);
})

app.on('error', function (err, ctx) {
  console.log(err);
  logger.error('server error', err, ctx);
});

module.exports = app.listen(config.port, () => {
  console.log(`Listening on http://localhost:${config.port}`);
});
