import { ParameterizedContext } from "koa";

export class IndexController {

  async show(ctx: ParameterizedContext["response.ctx"]): Promise<any> {
    await ctx.render("index", { "title": "koa-typescript-template" });
  }

}

export const indexController = Object.freeze(new IndexController());
