import { indexController } from "@controllers/IndexController";
import * as Router from 'koa-router';

const router = new Router();

router.get("/", (ctx) => indexController.show(ctx));

export default router;
