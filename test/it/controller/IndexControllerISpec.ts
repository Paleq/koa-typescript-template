import * as supertest from 'supertest';
import { Http } from '@status/codes';
import * as App from '../../../src/app';

const req = supertest(App);

describe('GET /', () => {

  test('should return OK', () => {
    return req.get('/')
      .expect(Http.Ok);
  });

});
