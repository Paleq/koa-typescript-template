import { JSDOM } from 'jsdom';
import * as Ejs from 'ejs';

export class TemplateHelper {

  static render(viewTemplatePath: string, data: Ejs.Data): Promise<Document> {
    const pHtml: Promise<string> = Ejs.renderFile<string>(viewTemplatePath, data);
    return pHtml.then(html => new JSDOM(html).window.document);
  }

}
