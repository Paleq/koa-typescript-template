import * as Path from 'path';
import { TemplateHelper } from '../helpers/TemplateHelper'

const viewTemplate = Path.join(__dirname, "../../../views/index.ejs");
const testViewData = { title: "Hello" }

describe("The index page", () => {

  test(`should have the title ${testViewData.title}`, () => {
    return TemplateHelper.render(viewTemplate, testViewData).then(doc => {
      const actual = doc.title;
      const expected = "Hello";

      expect(actual).toBe(expected);
    });
  });

  test(`should have the heading ${testViewData.title}`, () => {
    return TemplateHelper.render(viewTemplate, testViewData).then(doc => {
      const actual = doc.querySelector('h1').textContent;
      const expected = "Hello";

      expect(actual).toBe(expected);
    });
  });

  test(`should have a heading that matches the title`, () => {
    return TemplateHelper.render(viewTemplate, testViewData).then(doc => {
      const title = doc.title;
      const h1 = doc.querySelector('h1').textContent;

      expect(h1).toEqual(title);
    });
  });

  test("should have a paragraph", () => {
    return TemplateHelper.render(viewTemplate, testViewData).then(doc => {
      const actual = doc.querySelector('p').textContent;
      const expected = "EJS Welcome to Hello";

      expect(actual).toBe(expected);
    });
  });

});
