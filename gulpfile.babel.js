'use strict'

import '@babel/register';
import { preCleanTask, postCleanTask } from './assets/gulp/clean.js';
import { minifyTask } from './assets/gulp/minify.js';
import { transpileTask } from './assets/gulp/transpile.js';
import { concatTask } from './assets/gulp/concat.js';
import { sassTask } from './assets/gulp/sass.js';

let gulp = require('gulp');

gulp.task('default', gulp.series(
    preCleanTask,
    gulp.parallel(
        gulp.series(
            concatTask,
            transpileTask,
            minifyTask
        ),
        sassTask
    ),
    postCleanTask
));