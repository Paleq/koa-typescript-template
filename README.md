# KOA template (typescript)

Provides a basic starting point for a KOA project using typescript.

Features:
* Logic separated from routes in controllers
* Configured testing with Jest & JsDom (with examples)
* Some simple helpers to remove boilerplate

Todo:
* SASS asset management
* A more complete example project
* Feature switching

## Getting started

1. clone the app with `git@gitlab.com:Paleq/koa-typescript-template` (SSH) or `https://gitlab.com/Paleq/koa-typescript-template.git` (HTTPS)
2. install dependencies with `npm install`
3. compile typescipt code with `tsc`
4. run with `npm start`
5. View the welcome page on `http://localhost:3000/`

## Running tests

* Run `npm test`
* This will look for any **.ts** files in the **test** folder, excluding anything in **test/helpers**
